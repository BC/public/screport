import os
import pickle
import glob
from threading import Thread
import cv2
import numpy as np
import wx
from imutils import contours
import imutils
from pubsub import pub



#get resource path
RESOURCE_PATH = os.path.join(os.path.dirname(__file__),"data")

def fromResource(path):
    return os.path.join(RESOURCE_PATH,path)

#set threshold comparison values
COLOR_THRESHOLD = 3
PIXEL_THRESHOLD = 3

class SettingsManager():

    def __init__(self):
        self.path = fromResource("settings.pkl")
        if not os.path.isfile(self.path):
            dirPath = os.path.dirname(self.path)
            if not os.path.isdir(dirPath): os.mkdir(os.path.dirname(self.path))
            self.referencesPath, self.screenshotsPath = "",""
            self.saveSettings()
        else:
            with open(self.path, 'rb') as f:
                self.referencesPath, self.screenshotsPath = pickle.load(f)
    
    def saveSettings(self):
        with open(self.path, 'wb') as f:
            pickle.dump([self.referencesPath, self.screenshotsPath], f)


class ReferenceManager():

    def __init__(self,referencePath):
        self.images = {}
        self.changes = {}
        self.masks = {}
        self.path = referencePath

    def getGlobalMasks(self,shape):
        if shape not in self.masks:
            path = os.path.join(self.path,str(shape[1])+"x"+str(shape[0]))
            if not os.path.isdir(path): os.mkdir(path)
            path = os.path.join(path,"global.png")
            with open(ReferenceMaker.getDataPath(path), 'rb') as file:
                self.masks[shape] = pickle.load(file)[1]
        return self.masks[shape]

    def getReference(self,name,variantName,shape):
        path = os.path.join(self.path,str(shape[1])+"x"+str(shape[0]))

        if shape not in self.images:
            if not os.path.isdir(path): os.mkdir(path)
            self.images[shape] = {}
            
        path = os.path.join(path,name)

        if name not in self.images[shape]:
            self.images[shape][name] = {}

        if None not in self.images[shape][name]:
            isEmpty,reference = ReferenceMaker.getImage(path+".png",shape)
            with open(ReferenceMaker.getDataPath(path+".png"), 'rb') as file:
                masks = pickle.load(file)[1]
            self.images[shape][name][None] = [reference,masks,isEmpty]

        if variantName != None and variantName not in self.images[shape][name]:
            variantPath = path+"."+variantName+".png"
            isEmpty,variant = ReferenceMaker.getImage(variantPath,shape)
            with open(ReferenceMaker.getDataPath(variantPath), 'rb') as file:
                clips,masks = pickle.load(file)
            reference = ReferenceMaker.buildReference(self.images[shape][name][None][0].copy(),variant,clips)
            self.images[shape][name][variantName] = [reference,variant,clips,masks,isEmpty]
        
        return self.images[shape][name][variantName]

    def updateReference(self,name,shape,reference):
        self.changes[(shape,name,None)] = (False,None)
        for key in self.images[shape][name]:
            if key == None:
                self.images[shape][name][None] = [reference.copy(),self.images[shape][name][None][1],False]
            else:
                variant,clips,masks,isEmpty = self.images[shape][name][key][1:]
                reference = ReferenceMaker.buildReference(reference.copy(),variant,clips)
                self.images[shape][name][key] = [reference,variant,clips,masks,isEmpty]
    
    def updateVariant(self,name,variantName,shape,variant,clips):
        self.changes[(shape,name,variantName)] = (False,False)
        reference = ReferenceMaker.buildReference(self.images[shape][name][None][0].copy(),variant,clips)
        masks = self.images[shape][name][variantName][3]
        self.images[shape][name][variantName] = [reference,variant,clips,masks,False]

    def resetFromFile(self,name,variantName,shape):
        self.images[shape][name].pop(variantName)

    def applyChanges(self):
        for change in self.changes:
            if change[1] == None:
                path = os.path.join(self.path,str(change[0][1])+"x"+str(change[0][0]),"global.png")
                with open(ReferenceMaker.getDataPath(path), 'wb') as file:
                    pickle.dump([[],self.masks[change[0]]], file)
            elif change[2] == None:
                reference,masks = self.images[change[0]][change[1]][change[2]][:2]
                path = os.path.join(self.path,str(change[0][1])+"x"+str(change[0][0]),change[1]+".png")
                if self.changes[change][0] != None:
                    cv2.imwrite(path,reference)
                if self.changes[change][1] != None:
                    with open(ReferenceMaker.getDataPath(path), 'wb') as file:
                        pickle.dump([[],masks], file)
            else:
                variant,clips,masks = self.images[change[0]][change[1]][change[2]][1:4]
                path = os.path.join(self.path,str(change[0][1])+"x"+str(change[0][0]),change[1]+"."+change[2]+".png")
                if self.changes[change][0] != None:
                    cv2.imwrite(path,variant)
                if self.changes[change][1] != None:
                    with open(ReferenceMaker.getDataPath(path), 'wb') as file:
                        pickle.dump([clips,masks], file)


            

        
class ScreenshotManager(Thread):

    def __init__(self,sendState):
        Thread.__init__(self)
        self.sendState = sendState
        self.stopThread = False
        self.start()
 
    def run(self):
        if self.sendState: wx.CallAfter(pub.sendMessage, topicName="update")
        self.manager = SettingsManager()
        self.referenceManager = ReferenceManager(self.manager.referencesPath)
        self.files = []
        if os.path.isdir(self.manager.screenshotsPath):
            self.files = glob.glob(self.manager.screenshotsPath + '/**/*.png', recursive=True)

        self.screenshots = [Screenshot(path,self.referenceManager) for path in self.files]
        self.screenshots.sort(key=lambda screenshot: screenshot.id)
        
        total = str(len(self.files))
        inc = int(10000/(len(self.files))) if len(self.files) != 0 else 1000

        testClass = None
        testMethod = None
        for i,screenshot in enumerate(self.screenshots):
            screenshot.construcImages()
            if self.stopThread:
                break
            msg = ""
            if testClass == None or screenshot.testClass != testClass:
                testClass = screenshot.testClass
                testMethod = None
                msg = testClass + msg
            if self.sendState: wx.CallAfter(pub.sendMessage, topicName="update", msg=msg, pos=0)
            msg=""
            if testMethod == None or screenshot.testMethod != testMethod:
                testMethod = screenshot.testMethod
                msg = testMethod + msg

            if self.sendState: 
                wx.CallAfter(pub.sendMessage, topicName="update",msg=msg, pos=1)

                wx.CallAfter(pub.sendMessage, topicName="update", msg=screenshot.displayedName, pos=2)
                wx.CallAfter(pub.sendMessage, topicName="update", msg=screenshot.line, pos=3)

                msg = "✅" if screenshot.state else "❌"
                wx.CallAfter(pub.sendMessage, topicName="update", msg=msg, pos=4, inc=inc, loading=str(i+1)+"/"+total)

        if self.sendState: wx.CallAfter(pub.sendMessage, topicName="update", msg=None,pos=0, inc=10000)



            
class Screenshot():

    def __init__(self, path, referenceManager):
        self.path = path
        self.referenceManager = referenceManager

        self.id = ""
        self.name = ""
        self.displayedName = ""
        self.variantName = None
        self.line = ""
        self.testMethod = ""
        self.testClass = ""
        self.testClassName = ""

        self.getValFromPath()

        self.diff = None
        self.score = ""
        self.firstState = True
        self.state = None
        self.diffAreas = []
        self.variantAreas = []
        self.globalMasks = []
        self.referenceMasks = [] 
        self.variantMasks = []
        self.dimensions = (None,None,None)
        self.needMainReference = None
    
    def getValFromPath(self):
        path = os.path.join(self.path)
        nameInfos = os.path.basename(path).split(".")
        self.line = nameInfos[0]
        self.name = nameInfos[1]
        self.displayedName = self.name
        if len(nameInfos) > 3:
            self.variantName = nameInfos[2]
            self.displayedName = self.displayedName + " (" + self.variantName + ")"

        path = os.path.dirname(path)
        self.id = int(os.path.basename(path))+int(self.line)

        path = os.path.dirname(path)
        self.testMethod = os.path.basename(path)

        path = os.path.dirname(path)
        self.testClassName = os.path.basename(path)
        self.testClass = self.testClassName.split(".")[-1]

    def construcImages(self):
        self.image = cv2.imread(self.path)
        try: self.image.shape
        except: self.image = cv2.imread(fromResource("empty.png"))
        self.dimensions = self.image.shape
        self.globalMasks = self.referenceManager.getGlobalMasks(self.dimensions)
        self.referenceMasks = self.referenceManager.getReference(self.name,None,self.dimensions)[1]
        if self.variantName != None:
            self.variantAreas,self.variantMasks = self.referenceManager.getReference(self.name,self.variantName,self.dimensions)[2:4]
        self.compare()
    
    def isNeededMainReference(self):
        if self.variantName != None and self.referenceManager.getReference(self.name,self.variantName,self.dimensions)[4]:
            return self.referenceManager.getReference(self.name,None,self.dimensions)[2]
        return False
        
    def getReferenceBitmap(self):
        h,w = self.dimensions[:2]
        reference = self.referenceManager.getReference(self.name,self.variantName,self.dimensions)[0]
        return wx.Bitmap.FromBuffer(w, h, cv2.cvtColor(reference, cv2.COLOR_BGR2RGB))

    def getMainReferenceBitmap(self):
        h,w = self.dimensions[:2]
        reference = self.referenceManager.getReference(self.name,None,self.dimensions)[0]
        return wx.Bitmap.FromBuffer(w, h, cv2.cvtColor(reference, cv2.COLOR_BGR2RGB))

    def getScreenshotBitmap(self):
        h,w = self.dimensions[:2]
        return wx.Bitmap.FromBuffer(w, h, cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB))

    def getDiffBitmap(self):
        h,w = self.dimensions[:2]
        return wx.Bitmap.FromBuffer(w, h, cv2.cvtColor(self.diff, cv2.COLOR_BGR2RGB))

    def compare(self):
        image = self.image
        reference = self.referenceManager.getReference(self.name,self.variantName,self.dimensions)[0]

        if reference.shape != self.dimensions:
            print("the 2 images needs to have the same size: referenceSize",self.reference.shape," / screenshotSize",self.dimensions)
            shape = reference.shape
            image = np.zeros((shape[0],shape[1],shape[2]), np.uint8)
            image[:]=(255,255,255)

        before_gray = cv2.cvtColor(self.addMasks(self.image), cv2.COLOR_BGR2GRAY)
        after_gray = cv2.cvtColor(self.addMasks(reference), cv2.COLOR_BGR2GRAY)

        shape = reference.shape
        self.diff = cv2.absdiff(before_gray,after_gray)
        thresh = cv2.threshold(self.diff, COLOR_THRESHOLD, 255, cv2.THRESH_BINARY)[1]
        mask = cv2.inRange(self.diff, np.array([0]), np.array([0]))
        self.diff = cv2.applyColorMap((255 - self.diff)*10, cv2.COLORMAP_AUTUMN)
        self.diff[mask > 0] = (255)
        contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        maxRect = (0,0,self.dimensions[1],self.dimensions[0])
        self.diffAreas = RectangleMerger(contours,20,maxRect).boundingBoxes
        self.diffAreas = [((x,y),(x + w, y + h)) for [x,y,w,h] in self.diffAreas if w > PIXEL_THRESHOLD or h > PIXEL_THRESHOLD]

        self.score = "{:.2f}%".format((1 - np.sum(self.diff == 0) / (shape[0]*shape[1])) * 100)
        if len(self.diffAreas) == 0 and (self.variantName == None or len(self.variantAreas) != 0):
            if self.firstState == True: self.state = True
            else: self.state = None
        else:
            self.firstState = False
            self.state = False
            self.saveImgsForReports(reference)

    def saveImgsForReports(self,reference):
        path = os.path.join(os.path.dirname(SettingsManager().screenshotsPath),"failures",self.testClassName,self.testMethod)
        if not os.path.isdir(path): os.makedirs(path)
        ref = cv2.cvtColor(reference.copy(), cv2.COLOR_BGR2BGRA)
        capt = cv2.cvtColor(self.image.copy(), cv2.COLOR_BGR2BGRA)
        diff = cv2.cvtColor(self.diff.copy(), cv2.COLOR_BGR2BGRA)
        for (x1,y1),(x2,y2) in self.diffAreas:
            cv2.rectangle(ref, (x1, y1), (x2, y2), (0,0,255,255), 2)
            cv2.rectangle(capt, (x1, y1), (x2, y2), (0,0,255,255), 2)
        for (x1,y1),(x2,y2) in self.variantAreas:
            cv2.rectangle(ref, (x1, y1), (x2, y2), (255,0,0,255), 2)
            cv2.rectangle(capt, (x1, y1), (x2, y2), (255,0,0,255), 2)
        for (x1,y1),(x2,y2) in self.referenceMasks+self.variantMasks+self.globalMasks:
            black_rect = np.zeros((y2-y1,x2-x1,4), dtype=np.uint8)
            black_rect[:]=(0,0,0,255)
            ref_rect = ref[y1:y2, x1:x2]
            ref_res = cv2.addWeighted(ref_rect, 0.4, black_rect, 0.6, 0.0)
            ref[y1:y2, x1:x2] = ref_res
            capt_rect = capt[y1:y2, x1:x2]
            capt_res = cv2.addWeighted(capt_rect, 0.4, black_rect, 0.6, 0.0)
            capt[y1:y2, x1:x2] = capt_res

        (h,w),m = ref.shape[:-1],15
        txtOffset = cv2.getTextSize("hp", cv2.FONT_HERSHEY_SIMPLEX, 2, 4)[0][1] +m
        background = np.zeros((h+txtOffset+3*m,w*3+4*m,4), np.uint8)
        for i,(img,title) in enumerate(zip([ref,capt,diff],["reference","capture","difference"])):
            (x1,y1),(x2,y2) = (m+(w+m)*i,m),((w+m)*(i+1),h+m)
            background[y1:y2, x1:x2] = img
            ts = cv2.getTextSize(title, cv2.FONT_HERSHEY_SIMPLEX, 2, 4)[0]
            pos = (round((x1+x2-ts[0])/2),h+2*m+ts[1])
            cv2.putText(background,title, pos, cv2.FONT_HERSHEY_SIMPLEX, 2,(255,255,255,255),8,2)
            cv2.putText(background,title, pos, cv2.FONT_HERSHEY_SIMPLEX, 2,(0,0,0,255),4,2)
        cv2.imwrite(os.path.join(path,os.path.basename(self.path)),background)

    def addMasks(self,image):
        img = image.copy()
        for pt1,pt2 in self.globalMasks+self.referenceMasks+self.variantMasks:
            img = cv2.rectangle(img, pt1=pt1, pt2=pt2, color=(0,0,0), thickness= -1)
        return img

    def changeImageToReference(self):
        self.referenceManager.updateReference(self.name,self.dimensions,self.image)

    def changeImageToVariant(self):
        maxRect = (0,0,self.dimensions[1],self.dimensions[0])
        areas = [RectangleMerger.inset([x,y,w,h],5,maxRect) for ((x,y),(w,h)) in self.diffAreas]
        areas = RectangleMerger.merge_rectangles(areas+[[x,y,w,h] for ((x,y),(w,h)) in self.variantAreas],0,maxRect)
        self.variantAreas.clear()
        self.variantAreas += [((x,y),(w,h)) for [x,y,w,h] in areas]
        self.referenceManager.updateVariant(self.name,self.variantName,self.dimensions,self.image,self.variantAreas)

    def updateVariantAreas(self):
        variant = self.referenceManager.images[self.dimensions][self.name][self.variantName][1]
        self.referenceManager.updateVariant(self.name,self.variantName,self.dimensions,variant,self.variantAreas)

    def undoVariant(self):
        self.referenceManager.resetFromFile(self.name,self.variantName,self.dimensions)
        self.variantAreas,self.variantMasks = self.referenceManager.getReference(self.name,self.variantName,self.dimensions)[2:4]
        print(self.variantAreas)

    def undoReference(self):
        self.referenceManager.resetFromFile(self.name,None,self.dimensions)
        self.referenceMasks = self.referenceManager.getReference(self.name,None,self.dimensions)[1]

    def refHasChanged(self):
        return (self.dimensions,self.name,None) in self.referenceManager.changes

    def varHasChanged(self):
        return self.variantName != None and (self.dimensions,self.name,self.variantName) in self.referenceManager.changes

    def refHasUntreatedChanges(self):
        if self.refHasChanged():
            change = self.referenceManager.changes[(self.dimensions,self.name,None)]
            if change[0] == False or change[1] == False: return True
        return False
    
    def varHasUntreatedChanges(self):
        if self.varHasChanged():
            change = self.referenceManager.changes[(self.dimensions,self.name,self.variantName)]
            if change[0] == False or change[1] == False: return True
        return False



class EmptyScreenshot(Screenshot):
    def __init__(self):
        super().__init__(fromResource("empty.png"),None)
        self.image = cv2.imread(self.path)
        self.dimensions = self.image.shape
        self.reference = self.image
        self.diff = self.image
    
    def getValFromPath(self):
        self.referencePath = self.path

    def construcImages(self):
        pass

    def getReferenceBitmap(self):
        return self.getScreenshotBitmap()

    def getScreenshotBitmap(self):
        h,w = self.dimensions[:2]
        return wx.Bitmap.FromBuffer(w, h, cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB))

class ReferenceMaker():

    def buildReference(reference,variant,clips):
        for (x1,y1),(x2,y2) in clips:
            croped_img = variant[y1:y2, x1:x2]
            reference[y1:y2, x1:x2] = croped_img
        return reference

    def getImage(path,shape):
        if os.path.isfile(path): return False,cv2.imread(path)
        emptyImg = np.zeros((shape[0],shape[1],shape[2]), np.uint8)
        emptyImg[:]=(255,255,255)
        return True,emptyImg

    def getDataPath(path):
        dataPath = os.path.join(os.path.dirname(path),"data")
        if not os.path.isdir(dataPath): os.mkdir(dataPath)
        dataPath = os.path.join(dataPath,os.path.basename(path)[:-3]+"pkl")
        if not os.path.isfile(dataPath):
            with open(dataPath, 'wb') as file:
                pickle.dump([[],[]], file)
        return dataPath


        
class RectangleMerger():
    def __init__(self,contours,margin,max):
        cnts = imutils.grab_contours(contours)
        if len(cnts) == 0:
            self.boundingBoxes = []
        elif len(cnts) == 1:
            cnts = contours[0] if len(contours) == 2 else contours[1]
            self.boundingBoxes=[cv2.boundingRect(c) for c in cnts]
        else:
            (cnts, boundingBoxes) = imutils.contours.sort_contours(cnts, method="left-to-right")
            self.boundingBoxes = list(boundingBoxes)
            self.boundingBoxes = RectangleMerger.merge_rectangles(self.boundingBoxes,margin,max)

    def union(a,b):
        x = min(a[0], b[0])
        y = min(a[1], b[1])
        w = max(a[0]+a[2], b[0]+b[2]) - x
        h = max(a[1]+a[3], b[1]+b[3]) - y
        return [x, y, w, h]

    def intersect(a,b):
        x = max(a[0], b[0])
        y = max(a[1], b[1])
        w = min(a[0]+a[2], b[0]+b[2]) - x
        h = min(a[1]+a[3], b[1]+b[3]) - y
        if w<0 or h<0: 
            return False
        return True

    def inset(a,margin,maxRect):
        x = max(a[0]-margin,maxRect[0])
        y = max(a[1]-margin,maxRect[1])
        w = min(a[2]+margin,maxRect[2])
        h = min(a[3]+margin,maxRect[3])
        return [x,y,w,h]

    def merge_rectangles(rec,margin,max):
        tested = [False for i in range(len(rec))]
        final = []
        i = 0
        while i < len(rec):
            if not tested[i]:
                j = i+1
                while j < len(rec):
                    rec1 = RectangleMerger.inset(rec[i],margin,max)
                    rec2 = RectangleMerger.inset(rec[j],margin,max)
                    if not tested[j] and RectangleMerger.intersect(rec1,rec2):
                        rec[i] = RectangleMerger.union(rec[i], rec[j])
                        tested[j] = True
                        j = i
                    j += 1
                final += [rec[i]]
            i += 1

        return final

