import wx
import argparse
from MainFrame import MainFrame
from ProgressFrame import ProgressFrame
from SettingsFrame import SettingsFrame
from library import ScreenshotManager, SettingsManager



class MyApp(wx.App):
    def __init__(self):
        super().__init__(clearSigInt=True)

        self.settingsFrame = SettingsFrame(self.refreshProgressDialog)
        self.frame = None
        self.launchProgressFrame()

    def launchProgressFrame(self):
        self.screenshotManager = ScreenshotManager(True)

        self.progress = ProgressFrame()
        self.progress.settings.Bind(wx.EVT_BUTTON, self.OnSettings)
        self.progress.show.Bind(wx.EVT_BUTTON, self.OnMainFrame)
        self.progress.Bind(wx.EVT_CLOSE, self.OnClose)
        self.progress.Show()

    def OnSettings(self,event):
        try:
            self.settingsFrame.Hide()
            self.settingsFrame.Show()
        except: 
            self.settingsFrame = SettingsFrame(self.refreshProgressDialog)
            self.settingsFrame.Show()

    def OnMainFrame(self,event):
        self.progress.autoShow = True
        self.progress.Close()
        self.frame = MainFrame(self.screenshotManager.screenshots,self.OnSettings)
        self.frame.Show()
        if self.settingsFrame and self.settingsFrame.IsShown(): self.OnSettings(None)

    def refreshProgressDialog(self):
        openFrame = False
        if self.frame != None:
            self.frame.Destroy()
            openFrame = True
        else:
            self.screenshotManager.stopThread = True
            self.progress.Destroy()

        self.launchProgressFrame()
        self.progress.autoShow = openFrame
        self.OnSettings(None)

    def OnClose(self, event):
        if event.CanVeto():
            if self.progress.cancelMode or self.progress.done or self.progress.autoShow:
                self.screenshotManager.stopThread = True
                event.Skip()
                if self.settingsFrame: self.settingsFrame.Close()
                return
            msg = "Some conflicts have been detected"
            dlg = wx.MessageDialog(self.progress, "Are you sure to quit ?", msg, wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
            if dlg.ShowModal() != wx.ID_YES:
                event.Veto()
                return
        self.screenshotManager.stopThread = True
        event.Skip()
        if self.settingsFrame: self.settingsFrame.Close()
        


if __name__ == '__main__':
    parser = argparse.ArgumentParser('screport',description='screport - The comparison tool to check and manage the conform display of any app')
    parser.add_argument('-rp', '--reference-path',
                        help='The path to search for reference files',
                        required=False,
                        dest='referencePath')
    parser.add_argument('-sp', '--screeenshot-path',
                        help='The path to search for screenshot files',
                        required=False,
                        dest='screenshotPath')
    parser.add_argument('-wd', '--windowed',
                        help='Launch GUI or just compare screenshot',
                        required=False,
                        dest='windowed')
    args = parser.parse_args()
    settingsmanager = SettingsManager()
    if args.referencePath != None: settingsmanager.referencesPath = args.referencePath
    if args.screenshotPath != None: settingsmanager.screenshotsPath = args.screenshotPath
    settingsmanager.saveSettings()
    
    if args.windowed=="false":
        ScreenshotManager(False)
    else:
        app = MyApp()
        app.MainLoop()
        