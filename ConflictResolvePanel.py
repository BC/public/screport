import wx
from ImagePreview import ReferenceContainer, ScreenshotContainer
from library import EmptyScreenshot,fromResource


class ConflictResolvePanel(wx.Panel):

    def __init__(self, parent, screenshots):
        super().__init__(parent=parent)

        self.screenshots=screenshots
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.midlePanel = MidlePanel(self)

        screenshot = EmptyScreenshot()
        self.referencePreview = ReferenceContainer(self,screenshot,self.midlePanel.updateChangeBtn)
        self.screenshotPreview = ScreenshotContainer(self,screenshot,self.midlePanel.updateChangeBtn)
        self.referencePreview.brother = self.screenshotPreview
        self.screenshotPreview.brother = self.referencePreview

        compareSizer = wx.BoxSizer(wx.HORIZONTAL)
        compareSizer.Add(self.referencePreview,1,wx.EXPAND)
        compareSizer.Add(self.midlePanel,0,wx.EXPAND)
        compareSizer.Add(self.screenshotPreview,1,wx.EXPAND)

        window_border = 5

        self.bottomPanel = BottomPanel(self)

        mainSizer.Add((0,0),0,wx.TOP,window_border)
        mainSizer.Add(compareSizer,1,wx.LEFT|wx.RIGHT|wx.EXPAND,window_border)
        mainSizer.Add(self.bottomPanel,0,wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.EXPAND,window_border)
        
        leftBorder = wx.Panel(self,size=(1,0))
        leftBorder.SetBackgroundColour((220,220,220))
        sizer.Add(leftBorder,0,wx.EXPAND)
        sizer.Add(mainSizer,1,wx.EXPAND)
        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()

    def updateData(self,screenshot):
        self.referencePreview.show.Label = "h ide" + self.referencePreview.show.Label[5:]
        self.referencePreview.updateImage(screenshot)
        self.referencePreview.addMask.SetValue(False)
        self.referencePreview.OnToggle(None)
        self.midlePanel.changeState(screenshot.state)
        self.screenshotPreview.show.Label = "s how" + self.screenshotPreview.show.Label[5:]
        self.screenshotPreview.updateImage(screenshot)

class MidlePanel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)
        
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.stateIcon = wx.StaticBitmap(self,bitmap=wx.Image(fromResource("false_icon.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap(), size=(50,50))
        self.changeButton = wx.Button(self,label="<<")
        self.nextButton = wx.Button(self,label="next conflict ->")
        buttonBorder = 10

        mainSizer.Add((0,0),1,wx.EXPAND)
        mainSizer.Add(self.stateIcon,0,wx.CENTER|wx.LEFT|wx.RIGHT,buttonBorder)
        mainSizer.Add((0,0),1,wx.EXPAND)
        mainSizer.Add((0,0),1,wx.EXPAND)
        mainSizer.Add(self.changeButton,0,wx.CENTER|wx.LEFT|wx.RIGHT,buttonBorder)
        mainSizer.Add((0,0),1,wx.EXPAND)
        mainSizer.Add((0,0),1,wx.EXPAND)
        mainSizer.Add(self.nextButton,0,wx.CENTER|wx.LEFT|wx.RIGHT,buttonBorder)
        mainSizer.Add((0,0),1,wx.EXPAND)
        
        self.SetSizer(mainSizer)
        self.Layout()


    def changeState(self,state):
        if state == None:
            self.stateIcon.SetBitmap(wx.Image(fromResource("none_icon.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        elif state == True:
            self.stateIcon.SetBitmap(wx.Image(fromResource("true_icon.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        elif state == False:
            self.stateIcon.SetBitmap(wx.Image(fromResource("false_icon.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.stateIcon.SetSize((50,50))

    def updateChangeBtn(self,screenshot):
        if screenshot.variantName == None or self.Parent.referencePreview.show.Label[0] == "s":
            self.changeButton.Label = "undo" if screenshot.refHasChanged() else "<<"
        else:
            self.changeButton.Label = "undo" if screenshot.varHasChanged() else "<<"


class BottomPanel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)

        self.previousFunc = None
        self.nextFunc = None
        
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        buttonmargin = 10
        
        topSizer = wx.BoxSizer(wx.HORIZONTAL)
        
        self.prevButton = wx.Button(self,label="<- previous")
        self.nextButton = wx.Button(self,label="next ->")

        topSizer.Add(self.prevButton,0,wx.LEFT|wx.BOTTOM|wx.TOP,buttonmargin)
        topSizer.Add((0,0),wx.EXPAND)
        topSizer.Add(self.nextButton,0,wx.RIGHT|wx.BOTTOM|wx.TOP,buttonmargin)

        bottomSizer = wx.BoxSizer(wx.HORIZONTAL)
        
        self.refreshButton = wx.Button(self,label="refresh")
        self.applyButton = wx.Button(self,label="apply")
        self.applyButton.Disable()
        self.applyButton.Bind(wx.EVT_BUTTON,self.onApply)
        self.closeButton = wx.Button(self,label="close")

        bottomSizer.Add((0,0),wx.EXPAND)
        bottomSizer.Add(self.refreshButton,0,wx.RIGHT|wx.BOTTOM|wx.TOP,buttonmargin)
        bottomSizer.Add(self.applyButton,0,wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.TOP,buttonmargin)
        bottomSizer.Add(self.closeButton,0,wx.LEFT|wx.BOTTOM|wx.TOP,buttonmargin)
        bottomSizer.Add((0,0),wx.EXPAND)

        mainSizer.Add(topSizer,0,wx.EXPAND)
        mainSizer.Add(bottomSizer,0,wx.EXPAND)

        self.SetSizer(mainSizer)
        self.Layout()

    def onApply(self,event):
        self.Parent.referencePreview.screenshot.referenceManager.applyChanges()
        self.applyButton.Disable()