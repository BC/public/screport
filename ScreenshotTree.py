import wx

from library import EmptyScreenshot,fromResource


class ScreenshotTree(wx.Panel): 
            
    def __init__(self, parent, screenshots ,updateFunction, main_view): 
        super().__init__(parent)
		
        self.screenshots = screenshots
        self.mainRefresh = updateFunction
        self.main_view = main_view

        self.prevBtn = main_view.bottomPanel.prevButton
        self.prevBtn.Bind(wx.EVT_BUTTON, self.previousItem)
        self.nextBtn = main_view.bottomPanel.nextButton
        self.nextBtn.Bind(wx.EVT_BUTTON, self.nextItem)
        self.nextConflictBtn = main_view.midlePanel.nextButton
        self.nextConflictBtn.Bind(wx.EVT_BUTTON, self.nextConflictItem)
        self.changeButton = main_view.midlePanel.changeButton
        self.changeButton.Bind(wx.EVT_BUTTON,self.onChange)
        self.savedChanges = None
        self.refreshBtn = main_view.bottomPanel.refreshButton
        self.refreshBtn.Bind(wx.EVT_BUTTON,self.onRefresh)
            

        box = wx.BoxSizer(wx.VERTICAL) 

        self.tree = wx.TreeCtrl(self,size = (100,0), style=wx.TR_DEFAULT_STYLE | wx.TR_HIDE_ROOT)

        self.constructTree()

        self.updateBtnState()

        box.Add(self.tree,1,wx.EXPAND) 
        self.SetSizer(box) 
        self.Fit() 
		
        self.Centre() 
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.onSelect, self.tree) 
        self.Show(True)  
        
        if len(self.screenshotItems) != 0:
            self.tree.SelectItem(self.tree.GetFirstChild(self.tree.GetFirstChild(self.tree.GetFirstChild(self.tree.RootItem)[0])[0])[0])


    def constructTree(self):
        self.currentScreenshotIndex = 0
        self.screenshotItems = []
        testClass = None
        testMethod = None
        self.root = self.tree.AddRoot("TestSuite")

        image_list = wx.ImageList(16, 16)
        self.true_ic = image_list.Add(wx.Image(fromResource("true_small_icon.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.false_ic = image_list.Add(wx.Image(fromResource("false_small_icon.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.none_ic = image_list.Add(wx.Image(fromResource("none_small_icon.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.tree.AssignImageList(image_list)
        
        for screenshot in self.screenshots:
            if testClass == None or screenshot.testClass != self.tree.GetItemText(testClass):
                testClass = self.tree.AppendItem(self.root,screenshot.testClass)
                testMethod = None
            if testMethod == None or screenshot.testMethod != self.tree.GetItemText(testMethod):
                testMethod = self.tree.AppendItem(testClass,screenshot.testMethod)
            item = self.tree.AppendItem(testMethod,screenshot.displayedName)
            self.updateTreeImages(item,screenshot.state)
            self.screenshotItems.append(item)

        self.tree.ExpandAll()
		
        
    def getCurrentScreenshot(self):
        if len(self.screenshotItems) > 0:
            return self.screenshots[self.currentScreenshotIndex]
        return EmptyScreenshot()

    def previousItem(self,event):
        if self.currentScreenshotIndex > 0:
            self.currentScreenshotIndex += -1
            self.tree.SelectItem(self.screenshotItems[self.currentScreenshotIndex])

    def nextItem(self,event):
        if self.currentScreenshotIndex < len(self.screenshotItems)-1:
            self.currentScreenshotIndex += 1
            self.tree.SelectItem(self.screenshotItems[self.currentScreenshotIndex])

    def nextConflictItem(self,event):
        state = None
        while state != False:
            if self.currentScreenshotIndex < len(self.screenshotItems)-1:
                self.currentScreenshotIndex += 1
                state = self.getCurrentScreenshot().state
            else:
                break
        self.tree.SelectItem(self.screenshotItems[self.currentScreenshotIndex])

    def onChange(self,event):
        self.main_view.bottomPanel.applyButton.Enable()
        screenshot = self.getCurrentScreenshot()
        if self.changeButton.Label == "<<":
            #mode: no reference, and variant defined
            if screenshot.isNeededMainReference():
                msg1 = "The variant screenshot \""+screenshot.displayedName+"\" has no main reference"
                msg2 = "You have to define the \""+screenshot.name+"\" main reference first"
                dlg = wx.MessageDialog(self, msg2, msg1, wx.OK | wx.OK_DEFAULT | wx.ICON_ERROR)
                dlg.ShowModal()
            #mode: update/create main reference
            elif screenshot.variantName == None:
                screenshot.changeImageToReference()
            else:
            #mode: update main reference from variant
                if self.main_view.referencePreview.show.Label[0] == "s":
                    dlg = wx.MessageDialog(self, "Do you want to continue ?", "You are about to modify the main reference", wx.YES | wx.NO | wx.YES_DEFAULT | wx.ICON_QUESTION)
                    if dlg.ShowModal() == wx.ID_YES:
                        screenshot.changeImageToReference()
                        #+ enlever zones variantes
            #mode: update/create variant
                else:
                    screenshot.changeImageToVariant()
            
            self.mainRefresh(self.currentScreenshotIndex)
            self.changeButton.SetLabel("undo")
        else:
            if screenshot.variantName == None or self.main_view.referencePreview.show.Label[0] == "s":
                screenshot.undoReference()
            else:
                screenshot.undoVariant()
            
            self.mainRefresh(self.currentScreenshotIndex)
            self.changeButton.SetLabel("<<")

    def onRefresh(self,event):
        changes = []
        for screenshot in self.screenshots:
            if screenshot.refHasUntreatedChanges():
                change = (screenshot.dimensions,screenshot.name,None)
                if change not in changes: changes.append(change)
            if screenshot.varHasUntreatedChanges():
                screenshot.updateVariantAreas()
                change = (screenshot.dimensions,screenshot.name,screenshot.variantName)
                if change not in changes: changes.append(change)
            if screenshot.refHasUntreatedChanges() or screenshot.varHasUntreatedChanges():
                screenshot.compare()
        for change in changes:
            imageChange,dataChange = None,None
            changeState = screenshot.referenceManager.changes[change]
            if changeState[0] != None: imageChange = True
            if changeState[1] != None: dataChange = False
            screenshot.referenceManager.changes[change] = (imageChange,dataChange)
        

        currentIndex = self.currentScreenshotIndex
        self.refreshTree()
        self.tree.SelectItem(self.screenshotItems[currentIndex])
        self.mainRefresh(self.currentScreenshotIndex)

    def updateTreeImages(self,item,state):
        match(state):
            case True:
                ic = self.true_ic
            case False:
                ic = self.false_ic
            case None:
                ic = self.none_ic
        self.tree.SetItemImage(item, ic, wx.TreeItemIcon_Normal)

    def updateBtnState(self):
        if self.currentScreenshotIndex >= len(self.screenshotItems)-1:
            self.nextBtn.Disable()
            self.nextConflictBtn.Disable()
        else:
            self.nextBtn.Enable()
            self.nextConflictBtn.Enable()

        if self.currentScreenshotIndex <= 0:
            self.prevBtn.Disable()
        else:
            self.prevBtn.Enable()
		
    def onSelect(self, event): 
        item = event.GetItem()
        try:
            self.currentScreenshotIndex = self.screenshotItems.index(item)
        finally:
            self.updateBtnState()
            self.main_view.midlePanel.updateChangeBtn(self.getCurrentScreenshot())
            self.mainRefresh(self.currentScreenshotIndex)

    def refreshTree(self):
        self.tree.Delete(self.tree.RootItem)
        self.constructTree()
        self.updateBtnState()
        if len(self.screenshotItems) != 0:
            self.tree.SelectItem(self.tree.GetFirstChild(self.tree.GetFirstChild(self.tree.GetFirstChild(self.tree.RootItem)[0])[0])[0])   
        else:
            self.mainRefresh(self.currentScreenshotIndex)