from functools import partial
import wx

class DrawPanel(wx.Panel):
    def __init__(self, *args, **kwargs):
        wx.Panel.__init__(self, *args, **kwargs)

        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnClick)
        self.Bind(wx.EVT_MOTION, self.OnMove)
        self.Bind(wx.EVT_LEFT_UP, self.OnRelease)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnWheel)
        self.Bind(wx.EVT_RIGHT_DOWN,self.OnRightClick)

        self.brother = None
        self.allRects = [[],[],[],[],[]] #diffAreas,variantAreas,refmasks,variantMasks,globalMasks
        self.rectType = 0
        self.factor = 1
        self.hide = False

        self.addingMode = 0
        self.posMin = None
        self.posMax = None
        self.mode = 4
        self.variantMode = True
        self.offset = (0,0),(0,0)

        self.dragPos = None

    def drawRect(self,dc,posMin,posMax,rectType):
        if self.hide: return
        if rectType < 2:
            color = "red" if rectType == 0 else "blue"
            dc.SetPen(wx.Pen(color,style=wx.SOLID))
            dc.SetBrush(wx.Brush(color, style=wx.TRANSPARENT))
        else:
            color = "blue" if rectType == 3 else "black"
            style = wx.TRANSPARENT if rectType == 4 else wx.SOLID
            dc.SetPen(wx.Pen(color,style=style))
            dc.SetBrush(wx.Brush((0,0,0,150), style=wx.SOLID))
        dc.DrawRectangle(wx.Rect(topLeft=posMin,bottomRight=posMax))

    def OnPaint(self, event):
        self.factor = self.Size[1]/self.GrandParent.shape[0]
        dc = wx.PaintDC(self)
        for rectType,rects in enumerate(self.allRects):
            for p1,p2 in rects:
                p1,p2 = self.realRectToDisplayRect(p1,p2)
                self.drawRect(dc,p1,p2,rectType)
        if self.posMin != None:
            self.drawRect(dc,self.posMin,self.posMax,self.rectType)

    def checkIfRectClick(self,position):
        for rectType,rects in enumerate(self.allRects[1:]):
            if not self.variantMode and rectType != 2 or self.variantMode and rectType != 1:
                for i,rect in enumerate(rects):
                    x,y = position
                    (xmin,ymin),(xmax,ymax) = self.realRectToDisplayRect(rect[0],rect[1])
                    offset = 3
                    if x >= xmin - offset and x <= xmax + offset and y >= ymin - offset and y <= ymax + offset:
                        self.rectType = rectType +1
                        self.brother.rectType = self.rectType
                        rects.pop(i)
                        self.posMin,self.posMax = (xmin,ymin),(xmax,ymax)
                        self.brother.posMin,self.brother.posMax = self.posMin,self.posMax
                        self.GrandParent.hasChanged(self.variantMode if rectType != 3 else None)
                        if x >= xmin - offset and x <= xmin + offset and y >= ymin - offset and y <= ymin + offset:
                            return 0
                        if x >= xmin - offset and x <= xmin + offset and y >= ymax - offset and y <= ymax + offset:
                            return 1
                        if x >= xmax - offset and x <= xmax + offset and y >= ymin - offset and y <= ymin + offset:
                            return 2
                        if x >= xmax - offset and x <= xmax + offset and y >= ymax - offset and y <= ymax + offset:
                            return 3
                        self.offset = (xmin-x,ymin-y),(xmax-x,ymax-y)
                        self.brother.offset = self.offset
                        return 4
        if self.addingMode > 1:
            self.rectType = self.addingMode
            self.brother.rectType = self.rectType
            self.posMin,self.posMax = position,position
            self.brother.posMin,self.brother.posMax = self.posMin,self.posMax
            self.GrandParent.hasChanged(self.variantMode)
            return 3
        return 5

    def OnClick(self, event):
        if self.brother == None : self.brother = self.GrandParent.brother.drawPanel

        #for drag
        if wx.GetKeyState(wx.WXK_CONTROL):
            self.dragPos = event.Position
            return

        #for draw
        self.mode = self.checkIfRectClick(event.Position) 
        self.Refresh()
        self.brother.Refresh()

    def OnRightClick(self,event):
        if self.brother == None : self.brother = self.GrandParent.brother.drawPanel

        id = (None,None)
        for rectType,rects in enumerate(self.allRects):
            if not self.variantMode and rectType != 3 or self.variantMode and rectType != 2:
                for i,rect in enumerate(rects):
                    x,y = event.Position
                    (xmin,ymin),(xmax,ymax) = self.realRectToDisplayRect(rect[0],rect[1])
                    if x >= xmin and x <= xmax and y >= ymin and y <= ymax:
                        id = (rectType,i)
                        break
            if id != (None,None): break
        
        if id != (None,None):
            menu = wx.Menu("convert to")
            for i,name in enumerate(["variant area","reference mask","variant mask","global mask"]):
                if i != id[0] -1 and (self.variantMode or (i != 0 and i!= 2)):
                    menu.Append(wx.MenuItem(menu,10+i,name))
                    self.Bind(wx.EVT_MENU, partial(self.OnRectConvert,id,i+1), id=10+i)
            self.PopupMenu(menu,event.Position)

    def OnRectConvert(self,id,newRectType,event):
        self.allRects[newRectType].append(self.allRects[id[0]].pop(id[1]))
        if newRectType in [1,3] or id[0] in [1,3]:
            self.GrandParent.hasChanged(True)
        if newRectType == 2 or id[0] == 2:
            self.GrandParent.hasChanged(False)
        if newRectType == 4 or id[0] == 4:
            self.GrandParent.hasChanged(None)
        self.Refresh()
        self.brother.Refresh()
            

    def OnMove(self, event):
        #for drag
        if wx.GetKeyState(wx.WXK_CONTROL) and self.dragPos != None: 
            dx,dy = (event.Position[0] - self.dragPos[0], event.Position[1] - self.dragPos[1])
            self.GrandParent.offset = (self.GrandParent.offset[0]+dx,self.GrandParent.offset[1]+dy)
            self.GrandParent.brother.offset = self.GrandParent.offset

            self.GrandParent.OnRefresh()
            self.GrandParent.brother.OnRefresh()
            return

        #for draw
        if self.posMin != None :
            (xmin,ymin),(xmax,ymax) = self.posMin,self.posMax
            match self.mode:
                case 0:
                    self.posMin = (event.Position[0],event.Position[1])
                    if self.posMin[0] > self.posMax[0]: self.posMin = (self.posMax[0],self.posMin[1])
                    if self.posMin[1] > self.posMax[1]: self.posMin = (self.posMin[0],self.posMax[1])
                case 1:
                    self.posMin = (event.Position[0],self.posMin[1])
                    self.posMax = (self.posMax[0],event.Position[1])
                    if self.posMin[0] > self.posMax[0]: self.posMin = (self.posMax[0],self.posMin[1])
                    if self.posMin[1] > self.posMax[1]: self.posMax = (self.posMax[0],self.posMin[1])
                case 2:
                    self.posMin = (self.posMin[0],event.Position[1])
                    self.posMax = (event.Position[0],self.posMax[1])
                    if self.posMin[0] > self.posMax[0]: self.posMax = (self.posMin[0],self.posMax[1])
                    if self.posMin[1] > self.posMax[1]: self.posMin = (self.posMin[0],self.posMax[1])
                case 3:
                    self.posMax = (event.Position[0],event.Position[1])
                    if self.posMin[0] > self.posMax[0]: self.posMax = (self.posMin[0],self.posMax[1])
                    if self.posMin[1] > self.posMax[1]: self.posMax = (self.posMax[0],self.posMin[1])
                case 4:
                    self.posMin = (event.Position[0]+self.offset[0][0],event.Position[1]+self.offset[0][1])
                    self.posMax = (event.Position[0]+self.offset[1][0],event.Position[1]+self.offset[1][1])

            sizeMax = self.GetSize()
            if self.posMin[0] < 0: 
                self.posMin,self.posMax = (0,self.posMin[1]),(xmax,self.posMax[1])
            elif self.posMax[0] > sizeMax[0]: 
                self.posMin,self.posMax = (xmin,self.posMin[1]),(sizeMax[0],self.posMax[1])
            if self.posMin[1] < 0: 
                self.posMin,self.posMax = (self.posMin[0],0),(self.posMax[0],ymax)
            elif self.posMax[1] > sizeMax[1]:
                self.posMin,self.posMax = (self.posMin[0],ymin),(self.posMax[0],sizeMax[1])

            self.brother.posMin, self.brother.posMax = self.posMin,self.posMax

            self.Refresh()
            self.brother.Refresh()

    def OnRelease(self, event):
        #for drag
        if self.dragPos != None:
            self.GrandParent.OnRefresh()
            self.GrandParent.brother.OnRefresh()
            self.dragPos = None
            return

        #for draw
        if self.mode != 5:
            if (self.posMin[0] == self.posMax[0] or self.posMin[1] == self.posMax[1]):
                self.Refresh()
                self.brother.Refresh()
            else:
                self.posMin, self.posMax = self.displayRectToRealRect(self.posMin, self.posMax)
                self.allRects[self.rectType].append((self.posMin,self.posMax))

            self.posMin = None
            self.posMax = None
            self.brother.posMin = None
            self.brother.posMax = None

    def OnWheel(self, event):
        if wx.GetKeyState(wx.WXK_CONTROL):
            factor = self.GrandParent.factor
            factor = factor/1.2 if event.GetWheelRotation() < 0 else factor*1.2
            if factor < 1: factor = 1
            elif factor > 4: factor = 4
            self.GrandParent.factor = factor
            self.GrandParent.brother.factor = self.GrandParent.factor
            self.GrandParent.OnRefresh()
            self.GrandParent.brother.OnRefresh()

    def realRectToDisplayRect(self,posMin,posMax):
        posMin = (round(posMin[0]*self.factor),round(posMin[1]*self.factor))
        posMax = (round(posMax[0]*self.factor),round(posMax[1]*self.factor))
        return posMin,posMax

    def displayRectToRealRect(self,posMin,posMax):
        posMin = (round(posMin[0]/self.factor),round(posMin[1]/self.factor))
        posMax = (round(posMax[0]/self.factor),round(posMax[1]/self.factor))
        return posMin,posMax

