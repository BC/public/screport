import wx
from DrawPanel import DrawPanel


class ImagePreview(wx.Panel):
    def __init__(self, parent, title, image, shape):
        super().__init__(parent)

        self.brother = None
        self.isInit = False

        if shape == (None,None,None): shape = (2200,1080,0)
        self.shape = shape

        mainSizer = wx.BoxSizer(wx.VERTICAL)

        title = wx.StaticText(self,label=title)
        self.imageContainer = wx.Panel(self)
        
        self.imageBitmap = wx.StaticBitmap(self.imageContainer,bitmap=image,size=(0,0))
        self.drawPanel = DrawPanel(self.imageContainer,pos=(0,0),style=wx.TRANSPARENT_WINDOW)

        self.dragPos = None
        self.factor = 1
        self.offset = (0,0)
        self.imageRatio = self.shape[1]/self.shape[0]
        self.imageContainer.Bind(wx.EVT_SIZE, self.OnRefresh)
        self.imageContainer.SetBackgroundColour((150,150,150))

        mainSizer.Add(title,0,wx.CENTER,5)
        mainSizer.Add(self.imageContainer,1,wx.EXPAND)

        self.SetSizer(mainSizer)
        mainSizer.Fit(self)
        self.Layout()

    def OnMouseOver(self,event):
        btn = event.GetEventObject()
        btn.Label = btn.Label[0]+btn.Label[2:]
        btn.Fit()
        self.updateBtnPos()

    def OnMouseUnOver(self,event):
        btn = event.GetEventObject()
        btn.Label = btn.Label[0]+" "+btn.Label[1:]
        btn.Size = (20,20)
        self.updateBtnPos()

    def updateBtnPos(self):
        pass

    def OnRefresh(self,event=None):
        w,h = self.imageContainer.GetSize()*self.factor
        x,y = (0,0)

        if w > h*self.imageRatio:
            x = w/self.factor
            y = h/self.factor
            w = round(h*self.imageRatio)
            x = round((x - w)/2)
            y = round((y - h)/2)
        else:
            x = w/self.factor
            y = h/self.factor
            h = round(w/self.imageRatio)
            w = w-3
            x = round((x - w)/2)
            y = round((y - h)/2)

        x,y = self.offsetManager(x,y)
        self.imageBitmap.SetSize(w,h)
        self.imageBitmap.SetPosition((x,y))
        self.drawPanel.SetSize(w,h)
        self.drawPanel.SetPosition((x,y))

        self.updateBtnPos()

    def offsetManager(self,x,y):
        xmax = self.imageContainer.Size[0]-self.imageBitmap.Size[0]
        ymax = self.imageContainer.Size[1]-self.imageBitmap.Size[1]
        dx,dy = self.offset
        if self.imageContainer.Size[0] < self.imageBitmap.Size[0]:
            if x + dx > 0: dx = -x
            elif x + dx < xmax: dx = xmax - x
        else:
            dx = 0
        if self.imageContainer.Size[1] < self.imageBitmap.Size[1]:
            if y + dy > 0: dy = -y
            elif y + dy < ymax: dy = ymax - y
        else:
            dy = 0
        self.offset = (round(dx),round(dy))
        x += self.offset[0]
        y += self.offset[1]
        return x,y
    
    def changeImage(self,image,shape):
        self.shape = shape
        self.factor = 1
        self.offset = (0,0)
        self.imageBitmap.SetBitmap(image)
        self.imageBitmap.Size = self.imageContainer.GetSize()
        self.imageRatio = self.shape[1]/self.shape[0]
        self.OnRefresh()

    def changeAreas(self,diffs,variants,refMasks,varMasks,globMasks):
        self.drawPanel.allRects = [diffs,variants,refMasks,varMasks,globMasks]


class ReferenceContainer(ImagePreview):
    def __init__(self, parent, screenshot,updateBtn):
        super().__init__(parent,"reference",screenshot.getReferenceBitmap(),screenshot.dimensions)

        self.changeAreas(screenshot.diffAreas,screenshot.variantAreas,screenshot.referenceMasks,screenshot.variantMasks,screenshot.globalMasks)
        
        self.updateChangeBtn = updateBtn
        self.isInit = True
        self.screenshot = screenshot
        self.show = wx.Button(self.imageContainer,label="h ide variant",pos=(0,0),size=(20,20))
        self.show.Bind(wx.EVT_ENTER_WINDOW,self.OnMouseOver)
        self.show.Bind(wx.EVT_LEAVE_WINDOW,self.OnMouseUnOver)
        self.show.Bind(wx.EVT_BUTTON,self.OnBtnClick)

        self.addMask = wx.ToggleButton(self.imageContainer,label="+ add mask",pos=(0,0),size=(20,20))
        self.addMask.Bind(wx.EVT_ENTER_WINDOW,self.OnMouseOver)
        self.addMask.Bind(wx.EVT_LEAVE_WINDOW,self.OnMouseUnOver)
        self.addMask.Bind(wx.EVT_TOGGLEBUTTON, self.OnToggle)

    def hasChanged(self,variant):
        changes = []
        if variant == True: changes.append((self.screenshot.dimensions,self.screenshot.name,self.screenshot.variantName))
        elif variant == False: changes.append((self.screenshot.dimensions,self.screenshot.name,None))
        else: 
            changes.append((self.screenshot.dimensions,None,None))
            for name in self.screenshot.referenceManager.images[self.screenshot.dimensions]:
                for variantName in self.screenshot.referenceManager.images[self.screenshot.dimensions][name]:
                    changes.append((self.screenshot.dimensions,name,variantName))

        for change in changes:
            imageChange = None if change not in self.screenshot.referenceManager.changes else self.screenshot.referenceManager.changes[change][0]
            self.screenshot.referenceManager.changes[change] = (imageChange,False)

        self.updateChangeBtn(self.screenshot)
        self.Parent.bottomPanel.applyButton.Enable()

    def updateImage(self, screenshot):
        self.screenshot = screenshot
        self.changeImage(screenshot.getReferenceBitmap(), screenshot.dimensions)
        self.changeAreas(screenshot.diffAreas,screenshot.variantAreas,screenshot.referenceMasks,screenshot.variantMasks,screenshot.globalMasks)
        self.drawPanel.variantMode = True if screenshot.variantName != None else False

    def OnBtnClick(self,event):
        btn = event.GetEventObject()
        if "show" in btn.Label:
            btn.Label = "hide" + btn.Label[4:]
            rect = self.imageBitmap.GetRect()
            self.updateImage(self.screenshot)
            self.drawPanel.allRects[1] = self.screenshot.variantAreas
            self.drawPanel.allRects[3] = self.screenshot.variantMasks
            self.imageBitmap.SetRect(rect)
            self.drawPanel.variantMode = True
            if self.addMask.GetValue(): self.drawPanel.addingMode = 3
        elif "hide":
            btn.Label = "show" + btn.Label[4:]
            rect = self.imageBitmap.GetRect()
            self.changeImage(self.screenshot.getMainReferenceBitmap(),self.screenshot.dimensions)
            self.drawPanel.allRects[1] = []
            self.drawPanel.allRects[3] = []
            self.imageBitmap.SetRect(rect)
            self.drawPanel.variantMode = False
            if self.addMask.GetValue(): self.drawPanel.addingMode = 2
        self.updateChangeBtn(self.screenshot)

    def OnToggle(self,event):
        if self.addMask.GetValue():
            if self.screenshot.variantName == None or self.show.Label[0] == "s":
                self.drawPanel.addingMode = 2
            else:
                self.drawPanel.addingMode = 3
        else:
            self.drawPanel.addingMode = 1

    def updateBtnPos(self):
        if self.isInit == False:
            return
        margin = 0
        if self.screenshot.variantName != None:
            xb,yb = (self.imageContainer.Size[0]-self.show.Size[0]-5),(self.imageContainer.Size[1]-self.show.Size[1]-5)
            self.show.Position = (xb,yb)
            self.show.Show()
            margin = self.show.Size[1] + 5
        else :
            self.show.Hide()
        xb,yb = (self.imageContainer.Size[0]-self.addMask.Size[0]-5),(self.imageContainer.Size[1]-self.addMask.Size[1]-margin-5)
        self.addMask.Position = (xb,yb)

        
class ScreenshotContainer(ImagePreview):
    def __init__(self, parent, screenshot, updateBtn):
        super().__init__(parent,"capture",screenshot.getScreenshotBitmap(),screenshot.dimensions)

        self.changeAreas(screenshot.diffAreas,screenshot.variantAreas,screenshot.referenceMasks,screenshot.variantMasks,screenshot.globalMasks)

        self.updateChangeBtn = updateBtn
        self.isInit = True
        self.screenshot = screenshot

        self.show = wx.Button(self.imageContainer,label="s how diffs",pos=(0,0),size=(20,20))
        self.show.Bind(wx.EVT_ENTER_WINDOW,self.OnMouseOver)
        self.show.Bind(wx.EVT_LEAVE_WINDOW,self.OnMouseUnOver)
        self.show.Bind(wx.EVT_BUTTON,self.OnBtnClick)

    def hasChanged(self,variant):
        changes = []
        if variant == True: changes.append((self.screenshot.dimensions,self.screenshot.name,self.screenshot.variantName))
        elif variant == False: changes.append((self.screenshot.dimensions,self.screenshot.name,None))
        else: 
            changes.append((self.screenshot.dimensions,None,None))
            for name in self.screenshot.referenceManager.images[self.screenshot.dimensions]:
                for variantName in self.screenshot.referenceManager.images[self.screenshot.dimensions][name]:
                    changes.append((self.screenshot.dimensions,name,variantName))

        for change in changes:
            imageChange = None if change not in self.screenshot.referenceManager.changes else self.screenshot.referenceManager.changes[change][0]
            self.screenshot.referenceManager.changes[change] = (imageChange,False)

        self.updateChangeBtn(self.screenshot)
        self.Parent.bottomPanel.applyButton.Enable()

    def updateImage(self, screenshot):
        self.screenshot = screenshot
        self.changeImage(screenshot.getScreenshotBitmap(), screenshot.dimensions)
        self.changeAreas(screenshot.diffAreas,screenshot.variantAreas,screenshot.referenceMasks,screenshot.variantMasks,screenshot.globalMasks)
        self.drawPanel.variantMode = True if screenshot.variantName != None else False

    def OnBtnClick(self,event):
        btn = event.GetEventObject()
        if "show" in btn.Label:
            btn.Label = "hide" + btn.Label[4:]
            self.drawPanel.hide = True
            self.drawPanel.Refresh()
            self.savedBitmap = self.imageBitmap.GetBitmap()
            rect = self.imageBitmap.GetRect()
            self.imageBitmap.SetBitmap(self.screenshot.getDiffBitmap())
            self.imageBitmap.SetRect(rect)
            self.OnRefresh()
        elif "hide":
            btn.Label = "show" + btn.Label[4:]
            self.drawPanel.hide = False
            self.drawPanel.Refresh()
            rect = self.imageBitmap.GetRect()
            self.imageBitmap.SetBitmap(self.savedBitmap)
            self.imageBitmap.SetRect(rect)
            self.OnRefresh()

    def updateBtnPos(self):
        if self.isInit == False:
            return
        xb,yb = (self.imageContainer.Size[0]-self.show.Size[0]-5),(self.imageContainer.Size[1]-self.show.Size[1]-5)
        self.show.Position = (xb,yb)