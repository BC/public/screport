import wx
from library import SettingsManager


class SettingsFrame(wx.Frame):
    def __init__(self,refreshFunc,parent=None):
        wx.Frame.__init__(self, parent)

        self.refreshFunc = refreshFunc
        self.__set_properties()
        self.__do_layout()

    def __set_properties(self):
        self.SetTitle("Settings")
        self.manager = SettingsManager()

    def __do_layout(self):
        separator = wx.Panel(self,size=(0,1))
        separator.SetBackgroundColour((220,220,220))

        self.applyBtn = wx.Button(self,label="Apply")
        self.applyBtn.Bind(wx.EVT_BUTTON, self.onApply)
        self.applyBtn.Disable()
        closeBtn = wx.Button(self,label="Close")
        closeBtn.Bind(wx.EVT_BUTTON, self.OnExit)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        
        self.refInput = Input(self,"  references :   ",self.manager.referencesPath, self.applyBtn)
        self.screenInput = Input(self,"  screenshots :",self.manager.screenshotsPath, self.applyBtn)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(wx.StaticText(self,label="Paths"),0,wx.LEFT | wx.TOP| wx.EXPAND, 10)
        sizer.Add(separator,0,wx.TOP | wx.EXPAND,5)
        sizer.Add(self.refInput, 0, wx.TOP | wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.EXPAND, 10)
        sizer.Add(self.screenInput, 0, wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.EXPAND, 10)

        bottomSizer = wx.BoxSizer(wx.HORIZONTAL)
        bottomSizer.Add(self.applyBtn,0,wx.RIGHT | wx.LEFT | wx.TOP | wx.BOTTOM | wx.CENTER, 10)
        bottomSizer.Add(closeBtn,0,wx.RIGHT | wx.LEFT | wx.TOP | wx.BOTTOM | wx.CENTER, 10)
        sizer.Add(bottomSizer,0,wx.CENTER)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()
        self.Center()

    def onApply(self,event):
        self.manager.referencesPath = self.refInput.textCtrl.GetValue()
        self.manager.screenshotsPath = self.screenInput.textCtrl.GetValue()
        self.manager.saveSettings()
        self.refreshFunc()
        self.applyBtn.Disable()

    def OnExit(self,event):
        self.Close()

    def OnClose(self,event):
        if event.CanVeto():
            if self.applyBtn.IsEnabled():
                dlg = wx.MessageDialog(self, 'Do you want to close anyway ?', 'You have not applied the changes', wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
                if dlg.ShowModal() != wx.ID_YES:
                    event.Veto()
                    return
            else:
                event.Skip()
                return
        event.Skip()
        

class Input(wx.Panel):
    def __init__(self, parent, label, currentDirectory, applyBtn):
        super().__init__(parent)

        self.currentDirectory = currentDirectory
        self.applyBtn = applyBtn

        mainSizer = wx.BoxSizer(wx.HORIZONTAL)

        mainSizer.Add(wx.StaticText(self,label=label,style=wx.CENTER),0,wx.RIGHT,10)
        self.textCtrl = wx.TextCtrl(self,value=self.currentDirectory,size=(300,22))
        self.textCtrl.Bind(wx.EVT_TEXT,self.onChange)
        mainSizer.Add(self.textCtrl,1)
        openFileDlgBtn = wx.Button(self, label="Browse")
        openFileDlgBtn.Bind(wx.EVT_BUTTON, self.onDir)
        mainSizer.Add(openFileDlgBtn,0,wx.LEFT,10)

        self.SetSizer(mainSizer)
        mainSizer.Fit(self)
        self.Layout()

    def onDir(self, event):
        """
        Show the DirDialog and print the user's choice to stdout
        """
        dlg = wx.DirDialog(self, "Choose a directory:",defaultPath=self.currentDirectory,style=wx.DD_DEFAULT_STYLE | wx.FD_CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
                self.currentDirectory = dlg.GetPath()
                self.textCtrl.SetValue(self.currentDirectory)
        dlg.Destroy()
        self.Refresh()

    def onChange(self,event):
        self.applyBtn.Enable()