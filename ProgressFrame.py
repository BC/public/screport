from pubsub import pub
import wx


class ProgressFrame(wx.Frame):
    def __init__(self):
        super().__init__(None,title="Screenshot Comparator")
        self.count = 0
        self.line = -1
        self.cancelMode = True
        self.done = True
        self.autoShow = False

        xSize,ySize = wx.DisplaySize()
        size = (int(xSize*0.75),int(ySize*0.33))
        self.SetSize(size)
        self.Center()
        
        self.log = wx.ListCtrl(self, size=(-1,100),style=wx.LC_REPORT|wx.BORDER_SUNKEN)
        self.log.InsertColumn(0, 'TestClass', width=300)
        self.log.InsertColumn(1, 'TestMethod', width=300)
        self.log.InsertColumn(2, 'Screenshot', width=300)
        self.log.InsertColumn(3, 'line', width=70)
        self.log.InsertColumn(4, 'state', width=70)

        self.progress = wx.Gauge(self, range=10000)
        self.loading = wx.StaticText(self,label="999/999",style=wx.ST_NO_AUTORESIZE | wx.ALIGN_CENTRE_HORIZONTAL)
        self.loading.Label = "0/0"

        self.settings = wx.Button(self,label="preferences")
        self.cancel = wx.Button(self,label="cancel")
        self.cancel.Bind(wx.EVT_BUTTON, self.onExit)
        self.close = wx.Button(self,label="close")
        self.close.Bind(wx.EVT_BUTTON, self.onExit)
        self.show = wx.Button(self,label="show")
        
        sizer = wx.BoxSizer(wx.VERTICAL)

        progressSizer = wx.BoxSizer(wx.HORIZONTAL)
        progressSizer.Add(self.progress, 1, wx.EXPAND)
        progressSizer.Add(self.loading,0, wx.EXPAND)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add(self.settings,0,wx.LEFT | wx.TOP | wx.BOTTOM,10)
        btnSizer.Add((0,0), 1, wx.EXPAND,10)
        btnSizer.Add(self.show, 0, wx.RIGHT | wx.TOP | wx.BOTTOM,10)
        btnSizer.Add(self.close, 0, wx.RIGHT | wx.TOP | wx.BOTTOM,10)
        btnSizer.Add(self.cancel, 0, wx.RIGHT | wx.TOP | wx.BOTTOM,10)

        sizer.Add(self.log,1,wx.CENTER | wx.EXPAND,10)
        sizer.Add(progressSizer,0,wx.LEFT | wx.EXPAND,10)
        sizer.Add(btnSizer,0,wx.EXPAND)
        self.SetSizer(sizer)
        
        pub.subscribe(self.updateProgress, topicName="update")

    def reset(self):
        self.count = 0
        self.line = -1
        self.cancelMode = True
        self.done = True

        self.progress.SetValue(0)
        self.log.ClearAll()
        self.loading.Label = "0/0"



    def updateProgress(self, msg=None, pos=0, inc = 0, loading=None):
        if self.count == 0:
            self.close.Hide()
            self.show.Hide()

        elif pos == 4 and msg == "❌": self.done = False
        if loading != None: self.loading.Label = loading
        self.count += inc

        if msg != None:
            if pos == 0:
                self.line += 1
                self.log.InsertItem(self.line,msg)
            else:
                self.log.SetItem(self.line,pos,msg)
            self.log.EnsureVisible(self.line)
        
        if self.count >= 10000 and self.cancel.IsShown():
            self.cancel.Hide()
            self.show.Position = self.close.Position
            self.close.Position = self.cancel.Position
            self.close.Show()
            self.show.Show()
            self.cancelMode = False
            if self.autoShow: 
                evt = wx.PyCommandEvent(wx.EVT_BUTTON.typeId,self.show.GetId())
                wx.PostEvent(self.show, evt)

        self.progress.SetValue(self.count)

    def onExit(self,event):
        self.Close()
    