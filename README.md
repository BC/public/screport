# screport

screenshot comparison GUI for UItesting


# dependencies

 - python >= 3.10
 - pip (or pip3 if the build machine has both python2 and python3)
 - wx (pip install wxPython or pip3 install wxPython)
 - cv2 (pip install opencv-python or pip3 install opencv-python)
 - skimage (pip install scikit-image or pip3 install scikit-image)
 - imutils (pip install imutils or pip3 install imutils)
 - pypubsub (pip install pypubsub or pip3 install pypubsub)
 

 
# usage

- ## _launch_

---

execute the python file ```launch.py```

**Example:**
```bash
python <path/to/dir>/screport/launch.py
```

<br/>

**Args:** (optional)

| Command                | Value                     | Description                                                  |
| ---------------------- | ------------------------- | ------------------------------------------------------------ |
| -h, --help             |                           | show an help message and exit                                |
| -rp, --reference-path  | 'path/to/references/dir'  | define the path where de look for the reference images       |
| -sp, --screenshot-path | 'path/to/screenshots/dir' | define the path where de look for the screenshots to compare |
| -wd, --windowed        | 'true' or 'false'         | launch with or without GUI                                   |

<br/>

> **Linphone Paths**
>
> - **linphone-android**
>   - reference path = ```linphone-android/app/src/androidTest/java/org/linphone/screenshots```
>   - screenshot path = ```linphone-android/app/build/reports/androidTests/connected/screenshots```
>
> - **linphone-iphone** _(not yet implemented)_

<details><summary>📷</summary><p>

![prewiew](readme-img/launch.png)
</p></details>

<br/>

 - ## _settings_

---

To change the reference and screenshot paths, open the settings window by clicking on the **_preference button_** from the loading window or from **_File > Preferences_** and fill in the new path.

> **_Note:_** you can also change them in command line on launch like seen before

<details><summary>📷</summary><p>

![prewiew](readme-img/settings1.png)
![prewiew](readme-img/settings2.png)
</p></details>
 	
<br/>

 - ## _editor_

---

Once the comparison is done, you can open the editor by clicking on the **_show_** button from the loading window, in order to see the conflicts detected and make changes or not.

![prewiew](readme-img/editor_legend.png)

<details><summary>Legend</summary><p>

>
> 1. Screenshot Tree
>
>    1.1 Test Class
>
>    1.2 Test Function
>
>    1.3 Status | Main Name | Variant Name 
>
> 2. Description | Similarity Score | Size | Index
>
> 3. Editor
>
>    3.1 Status
>
>    3.2 Set new screenshot to reference
>
>    3.3 Go next conflict
>
>    3.4 Go to previous comparison
>
>    3.5 Go to next comparison
>
>    3.6 Refresh comparisons after changes
>
>    3.7 Save changes on disk
>
>    3.8 Activate mask adding mode
>
>    3.9 Hide variant and show main reference <details><summary>3.10 Highlights different pixels</summary><p>![prewiew](readme-img/diff_preview.png)</p></details>
>
>   

</p></details>
<details><summary>Color Legend</summary><p>

![prewiew](readme-img/color_legend.png)
</p></details>

<br/>

> **_Note:_** you can change an area type by right clicking on it <details><summary>📷</summary><p>![prewiew](readme-img/right_click.png)</p></details>

<br/>

 - ## _storage rules_

---

_- each screenshot to compared should be stored like this:_

&emsp;&emsp;```<path/to/screenshot/dir>``` / ```<test-class-name>``` / ```<test-method-name>``` / ```<int-chronological-id>``` / ```<line>``` . ```<main-name>``` . ```<variant-name>``` . png

**Example:**
```bash
screenshots/org.linphone.call.OutgoingCallUITests/testViewDisplay/1673371781821/49.dialer_view.declined.png
```
<details><summary>📷</summary><p>

![prewiew](readme-img/screenshot_storage.png)
</p></details>

<br/>

---

_- reference storage is automatically generated:_

&emsp;&emsp;```<path/to/screenshot/dir>``` / ```<image-size>``` / ```<main-name>``` . ```<variant-name>``` . png

**Example:**
```bash
references/1080x2400/dialer_view.declined.png
```

> **_Note:_** each reference have a **'data'** file stored in ```<path/to/screenshot/dir>``` / ```<image-size>``` / data / ```<main-name>``` . ```<variant-name>``` .png

<details><summary>📷</summary><p>

![prewiew](readme-img/reference_storage.png)
</p></details>

<br/>

> **_Note:_** obviously on Windows the slashes '**/**' are replaced by backslashes '**\\**'

<br/>

 - ## _thresholds_

---

It is possible to adjust the threshold values ​​for the differences in colors and the size of the groups of different pixels from which the algorithm considers them as an error. To do this, you will need to edit the ```COLOR_THRESHOL``` and ```PIXEL_THRESHOLD``` variables declared at the beginning of the [library.py](library.py) file.

> **_Note:_** currently ```COLOR_THRESHOL=3``` (out of 255 in gray scale) and ```PIXEL_THRESHOLD=3``` (each area having one of its sides less than 3 pixels will be ignored)

<br/>
