import wx
from ConflictResolvePanel import ConflictResolvePanel
from ScreenshotTree import ScreenshotTree
from SettingsFrame import SettingsFrame
from library import EmptyScreenshot


class MainFrame(wx.Frame):
    def __init__(self,screenshots,settingsFunc):
        super().__init__(None)
        self.CreateStatusBar(4)
        self.SetStatusWidths([-1,70,100,50])

        self.screenshots = screenshots

        self.window = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_LIVE_UPDATE)
        self.main_view = ConflictResolvePanel(self.window,self.screenshots)
        self.list_view = ScreenshotTree(self.window,self.screenshots,self.updateData,self.main_view)

        file = wx.Menu()
        file.Append(1, '&Preferences\tCtrl+,', 'Open preferences')
        file.AppendSeparator()
        file.Append(2, '&Quit\tCtrl+Q', 'Quit')

        menubar = wx.MenuBar()
        menubar.Append(file, '&File')
        self.SetMenuBar(menubar)

        self.Bind(wx.EVT_MENU, settingsFunc, id=1)
        self.Bind(wx.EVT_MENU, self.OnExit, id=2)
        self.main_view.bottomPanel.closeButton.Bind(wx.EVT_BUTTON,self.OnExit)
        self.Bind(wx.EVT_CLOSE, self.OnClose)

        self.__set_properties()
        self.__do_layout()

    def __set_properties(self):
        self.SetTitle("Screenshot Comparator")
        
        xSize,ySize = wx.DisplaySize()
        size = (int(xSize*0.75),int(ySize*0.75))
        self.SetSize(size)
        self.Center()

    def __do_layout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.window.SplitVertically(self.list_view, self.main_view,int(self.GetSize()[0]*0.2))
        sizer.Add(self.window, 1, wx.EXPAND, 0)
        self.SetSizer(sizer)
        self.Layout()

    def updateData(self,screenshotIndex):
        screenshot = self.screenshots[screenshotIndex] if len(self.screenshots) != 0 else EmptyScreenshot()
        label = screenshot.testClass + "." + screenshot.testMethod + " : " + screenshot.displayedName
        self.SetStatusText(label,0)
        self.SetStatusText(screenshot.score,1)
        self.SetStatusText(str(screenshot.dimensions[1])+"x"+str(screenshot.dimensions[0]),2)
        self.SetStatusText(str(screenshotIndex+1)+"/"+str(len(self.screenshots)),3)
        self.main_view.updateData(screenshot)

    def OnSettings(self,event):
        self.settingsFrame.Show()

    def OnClose(self, event):
        if event.CanVeto() and self.main_view.bottomPanel.applyButton.IsEnabled():
            dlg = wx.MessageDialog(self, "Are you sure to quit ?", "You have unapplied changes", wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
            if dlg.ShowModal() != wx.ID_YES:
                event.Veto()
                return
        event.Skip()

    def OnExit(self, event):
        self.Close()


